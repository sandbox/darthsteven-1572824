<?php

/**
 * @file
 * Admin interface for Queue Mail.
 */

/**
 * The admin settings form for Queue Mail.
 */
function queue_mail_settings_form() {

  $form['queue_mail_keys'] = array(
    '#type' => 'textarea',
    '#title' => t('Mailkeys to queue'),
    '#description' => t('Enter each $mailkey on a separate line. Use <strong>*</strong> to do a wildcard match.') .'<br/>'. t('To find mail keys, use the second parameter in drupal_mail, e.g. user_mail, register_pending_approval_admin'),
    '#default_value' => variable_get('queue_mail_keys', ''),
  );

  return system_settings_form($form);
}
